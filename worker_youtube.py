import youtube_dl

class YoutubeWorker:

    def run(self, url, decorator=None):
        """
        Download the audio from a youtube video link.

        Examples
        --------
        >>> yt = YoutubeWorker()
        >>> yt.run("https://www.youtube.com/watch?v=rEQ-nk9hkHs") #doctest:+ELLIPSIS
        [youtube] rEQ-nk9hkHs: Downloading webpage
        ...
        [ffmpeg] Destination: ./music/Beep Short - Sound Effect.mp3
        ...

        >>> yt = YoutubeWorker()
        >>> yt.run("https://www.youtube.com/watch?v=rEQ-nk9hkHs", "patrick") #doctest:+ELLIPSIS
        [youtube] rEQ-nk9hkHs: Downloading webpage
        ...
        [ffmpeg] Destination: ./music/patrick | Beep Short - Sound Effect.mp3
        ...
        """
        file_name_decorator = "{} | ".format(decorator) if decorator else ""
        file_name_template = './music/{}%(title)s.%(ext)s'.format(file_name_decorator)

        ydl_opts = {
            'format': 'bestaudio/best',
            'outtmpl': file_name_template,
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'mp3',
                'preferredquality': '192',
            }],
        }

        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            ydl.download([url])


if __name__ == "__main__":
    import doctest
    doctest.testmod()
