import os, fnmatch

class Track:

    def __init__(self, tracks=None):
        self.MUSIC_DIR = 'music'
        self.tracks = tracks if tracks else os.listdir(self.MUSIC_DIR)


    def all(self):
        """
        Examples
        ---

        >>> tracks = Track(["track_A.mp3", "track_B.mp3", "track_C.mp3"]).all()
        >>> len(tracks)
        3

        """
        tracks = [track for track in self.tracks if fnmatch.fnmatch(track, '*.mp3')]
        return tracks

    def find(self, search_string):
        """
        Examples
        ---

        >>> tracks = Track(["track_A.mp3", "track_B.mp3", "track_C.mp3"]).find("B")
        >>> len(tracks)
        1

        >>> tracks = Track(["track_A.mp3", "track_B.mp3", "track_C.mp3"]).find("track")
        >>> len(tracks)
        3

        """
        tracks = self.tracks
        return [track for track in tracks if track.find(search_string) > -1]


    def create(self, url, decorator):
        """
        """
        #Workflow().download_song(url, decorator)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
