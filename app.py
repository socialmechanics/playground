from flask import Flask, request, render_template
from wtforms import Form, TextField
from models.track import Track

from flask_restful import Resource, Api, reqparse
import os, fnmatch

app = Flask(__name__)
api = Api(app)

class LinkForm(Form):
    dancer_name = TextField('Dancer\'s Name')
    youtube_url = TextField('Youtube URL')

@app.route("/", methods=['GET', 'POST'])
def home():
    form = LinkForm(request.form)
    if request.method == "POST":
        Track().create(form.youtube_url.data, form.dancer_name.data)
    return render_template('home.html', form=form)

# === API

class TrackListResource(Resource):
    def get(self):
        return { 'tracks': Track().all() }

class TrackResource(Resource):
    def get(self, search):
        return { 'tracks': Track().find(search) }

api.add_resource(TrackListResource, '/tracks')
api.add_resource(TrackResource, '/tracks/<search>')

if __name__ == "__main__":
    app.run(debug=True)
