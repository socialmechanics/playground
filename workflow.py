from rq import Queue
from redis import Redis
from worker_rssfeed import RSSFeedWorker
from worker_youtube import YoutubeWorker

class Workflow:

  def __init__(self):
    redis_conn = Redis()
    self.q = Queue(connection=redis_conn)

  def download_song(self, url, dancer_name):
      download_worker = self.q.enqueue(YoutubeWorker().run, url, dancer_name).id
      rss_worker = self.q.enqueue(RSSFeedWorker().run, depends_on=download_worker)
